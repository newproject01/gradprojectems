<%@ include file="/WEB-INF/views/includes/include.jsp" %>
<!doctype html>
<html lang="en">
<head>
    <title>Employee Management System</title>
    <!-- Bootstrap 4.5 CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <!-- Bootstrap JS Requirements -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
            integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"></script>

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Employee Management System</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-hover table-bordered mt-2">
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Joining Date</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
                <c:if test="${empty appList}">
                    <tr>
                        <td colspan="6">No Records Found</td>
                    </tr>
                </c:if>
                <c:if test="${not empty appList}">
                    <c:forEach var="app" items="${appList}">
                        <tr>
                            <td>${app.id}</td>
                            <td>${app.firstName}</td>
                            <td>${app.lastName}</td>
                            <td>${app.date}</td>

                                <%-- Update Employee	--%>
                            <td>
                                <form method="get" action="<%=request.getContextPath() %>/updateEmployee/${app.id}">
                                    <button type="submit" class="btn btn-warning">Update</button>
                                </form>
                            </td>

                                <%-- Delete Employee	--%>
                            <td>
                                <form method="post" action="<%=request.getContextPath() %>/deleteEmployee/${app.id}">
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>

                        </tr>
                    </c:forEach>
                </c:if>
            </table>
            <a href="<%=request.getContextPath() %>/addNewEmployee" class="btn btn-primary">Add New Employee</a>

        </div>
    </div>
</div>

</body>
</html>
