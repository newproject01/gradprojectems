
<!doctype html>
<html lang="en">
<head>
	<title>EMPLOYEE MANAGEMENT SYSTEM</title>
	<!-- Bootstrap 4.5 CSS-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">	
	<!-- Bootstrap JS Requirements -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	
</head>
<body>
	<div class="container">
		<div class="row">
		<form action="<%=request.getContextPath() %>/">
		<button type="submit" onclick="" class="btn btn-primary">Home</button>
		</form>
		
			<div class="col-12">
				<h1>Add New Employee</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<form method="post" action="<%=request.getContextPath() %>/addNewEmployee">
					<!-- add inputs for fn and ln -->
					<label for="id"><b>Employee ID :</b></label>
  					 <input type="text" id="id" name="id" placeholder="Enter your employee ID"><br>
					 <label for="firstName"><b>First name:</b></label>
  					 <input type="text" id="firstName" name="firstName" placeholder="Enter First Name"><br>
  					 <label for="lastName"><b>Last name:</b></label>
  					 <input type="text" id="lastName" name="lastName" placeholder="Enter Last Name"><br>
  					  <label for="date"><b>Joining Date:</b></label>
  					 <input type="text" id="date" name="date" placeholder="Enter joining date"><br>
					<button type="submit" class="btn btn-success">Submit</button>
					<button type="reset" class="btn btn-primary">Reset</button>
				</form>
			</div>
		</div>
	</div>
</body>