package com.missouristate.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.missouristate.domain.EmployeeDetails;
import com.missouristate.repository.EMSRepository;

@Service("EMSService")
public class EMSService {

    @Autowired
    EMSRepository emsRepo;

    public List<EmployeeDetails> getEmployeeData() {
        return (List<EmployeeDetails>) emsRepo.findAll();
    }

    // Save Employee Data
    @Transactional
    public void saveEmployeeData(EmployeeDetails employeedetails) {
        emsRepo.save(employeedetails);
    }

    // Edit employee Data
    @Transactional
    public void editEmployeeData(EmployeeDetails employeedetails) {
        emsRepo.save(employeedetails);
    }

    // Delete Employee Data
    @Transactional
    public void deleteEmployeeData(Integer id) {
        EmployeeDetails employeedetails = emsRepo.findById(id).get();
        emsRepo.delete(employeedetails);
    }

    // Delete Employee Data
    @Transactional
    public EmployeeDetails getEmployeeByID(Integer id) {
        return emsRepo.findById(id).get();
    }

}
 