package com.missouristate.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.missouristate.domain.EmployeeDetails;
import org.springframework.data.repository.query.Param;

public interface EMSRepository extends CrudRepository<EmployeeDetails, Integer>, EMSRepositoryCustom {
    // Spring Data abstract methods go here
    // public Application findByFirstName(String firstName); // select * from applications where first_name = firstName
    @Query(value = "UPDATE employeedetails SET first_name=:first_name,last_name=:last_name, date=:date WHERE id=:id", nativeQuery = true)
    int update(@Param("id") int id,
               @Param("first_name") String first_name,
               @Param("last_name") String last_name,
               @Param("date") String date
    );
}

