package com.missouristate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.missouristate.domain.EmployeeDetails;

import com.missouristate.domain.QEmployeeDetails;


@Repository
public class EMSRepositoryImpl extends QuerydslRepositorySupport implements EMSRepositoryCustom {
	
	QEmployeeDetails EmployeeDetails = QEmployeeDetails.employeeDetails;
	
	public EMSRepositoryImpl() {
		super(EmployeeDetails.class);
	}

	// Object Oriented SQL goes here...
	public EmployeeDetails getApplicationById(Integer id) {
		return (EmployeeDetails) from(EmployeeDetails)
				.where(EmployeeDetails.id.eq(id))
				.limit(1)
				.fetch();
	}
	
	public List<EmployeeDetails> getBootListByFirstName(String firstName) {
		return from(EmployeeDetails)
				.where(EmployeeDetails.firstName.eq(firstName))
				.orderBy(EmployeeDetails.id.asc())
				.fetch();
	}
	 
}
 