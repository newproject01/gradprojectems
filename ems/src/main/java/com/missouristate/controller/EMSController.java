package com.missouristate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.missouristate.domain.EmployeeDetails;
import com.missouristate.service.EMSService;

@Controller
public class EMSController {

    @Autowired
    EMSService emsService;

    @GetMapping(value = "/")
    public String getIndex(Model model) {
        List<EmployeeDetails> appList = emsService.getEmployeeData();
        model.addAttribute("appList", appList);
        return "index";
    }

    @GetMapping(value = "/addNewEmployee")
    public String getAddNewEmployee(Model model) {
        List<EmployeeDetails> appList = emsService.getEmployeeData();
        model.addAttribute("appList", appList);
        return "addNewEmployee";
    }

    @PostMapping(value = "/addNewEmployee")
    public String postAddNewEmployee(Model model, @ModelAttribute EmployeeDetails employeedetails) {
        System.out.println(employeedetails);
        emsService.saveEmployeeData(employeedetails);
        return "redirect:/";
    }

    // delete employee by id
    @PostMapping(value = "/deleteEmployee/{id}")
    public String postDeleteEmployee(Model model, @PathVariable int id) {
        emsService.deleteEmployeeData(id);
        return "redirect:/";
    }


    // get::  employee data for update by id
    @GetMapping(value = "/updateEmployee/{id}")
    public String getUpdateEmployee(Model model, @PathVariable int id) {
        EmployeeDetails employee = emsService.getEmployeeByID(id);
        model.addAttribute("employee", employee);
        return "updateEmployee";
    }

    // post:: update employee data
    @PostMapping(value = "/updateEmployee")
    public String postUpdateEmployee(Model model, @ModelAttribute EmployeeDetails employeedetails) {
//        System.out.println(employeedetails);
        emsService.editEmployeeData(employeedetails);
        return "redirect:/";
    }

}
