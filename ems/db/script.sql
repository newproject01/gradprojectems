CREATE database employee;

USE employee;

CREATE TABLE employeedetails
(
    id         INTEGER NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(64),
    last_name  VARCHAR(64),
    date       VARCHAR(64),
    PRIMARY KEY (id)
);

UPDATE employeedetails
SET id=?,
    first_name=?,
    last_name=?,
    date=?
WHERE id = ?